import { AppAngularVersionedQuestionsPage } from './app.po';

describe('app-angular-versioned-questions App', () => {
  let page: AppAngularVersionedQuestionsPage;

  beforeEach(() => {
    page = new AppAngularVersionedQuestionsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
